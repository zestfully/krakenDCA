require('dotenv').config()

const key = process.env.API_KEY // API Key
const secret = process.env.API_PRIVATE_KEY // API Private Key
const KrakenClient = require('kraken-api')
const kraken = new KrakenClient(key, secret)

let tries = 1
const MAX_TRIES = 10
const TIME_BETWEEN_TRIES = 60 * 60 * 1000

const getDaysInCurrentMonth = () => {
  const currentDate = new Date()
  currentDate.setMonth(currentDate.getMonth() + 1)
  currentDate.setDate(0)
  return currentDate.getDate()
}

const sleep = (ms) => {
	return new Promise((resolve) => {
		setTimeout(resolve, ms)
	})
}

const getPrice = () =>
  new Promise((resolve, reject) => {
    kraken.api('Ticker', { pair: 'XXBTZEUR' })
      .then((res) => resolve(
        res['result']['XXBTZEUR']['c'][0]
      ))
      .catch((err) => reject(err))
  })

const addOrder = (amount) => {
  const params = { 
    pair: 'XXBTZEUR', 
    type: 'buy', 
    ordertype: 'market', 
    volume: amount, 
    oflags: 'fcib' 
  }

  return kraken.api('AddOrder', params)
};

(async () => {
  const MONTHLY_EUR_AMOUNT = Number(process.env.MONTHLY_EUR_AMOUNT)
  const DAILY_EUR_AMOUNT = MONTHLY_EUR_AMOUNT / getDaysInCurrentMonth()

  while (tries <= MAX_TRIES) {
    if (tries > 1) await sleep(TIME_BETWEEN_TRIES)
    console.log(`Try #${tries}`)

    const price = await getPrice().catch(() => null)
    if (!price) {
      tries++
      continue
    }

    const btcAmountToBuy = Number((DAILY_EUR_AMOUNT / price).toFixed(8))
    const order = await addOrder(btcAmountToBuy).catch(() => null)
    if (!order) {
      tries++
      continue
    }

    console.log('Done');
    break
  }
})()
