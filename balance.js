require('dotenv').config()

const key = process.env.API_KEY // API Key
const secret = process.env.API_PRIVATE_KEY // API Private Key
const KrakenClient = require('kraken-api')
const kraken = new KrakenClient(key, secret)

const getBalance = () =>
  new Promise((resolve, reject) => {
    kraken.api('Balance')
      .then((res) => resolve(
        res.result.ZEUR
      ))
      .catch((err) => reject(err))
  });

(async () => {
  const balance = await getBalance().catch(() => null)
  console.log(balance)
})()
